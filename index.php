<?
session_start();
require_once 'bbissuemaker.php';
if ($_POST['titles']) {
	file_put_contents('1.txt', var_export($_POST, 1));
	$message = '';
	$content = '';
	foreach ($_POST['titles'] as $key => $title) {
		$title = trim($title);
		if ( ! $title)
			continue;
			
		$content = trim(strip_tags($_POST['content'][$key]));
		$hashes = trim($_POST['hashes'][$key]);
		$message .= 'Задание : '.$title."\n";
		$message .= $content;
		$message .= "Время выполнения: дней\nСумма: грн.\n";
		$message .= "-----------------------------------------------------------------------------\n";
		$tasks[] = array(
			'title' => $title,
			'content' => $content."\n\n".$hashes,
		);
	}
	file_put_contents('message.txt',  $message);
}
// Required fields login, password, repo name and commits per page
$login = $_POST['login'] ? ($_SESSION['login'] = $_POST['login']) : $_SESSION['login'];
$password = $_POST['password'] ? ($_SESSION['password'] = $_POST['password']) : $_SESSION['password'];
$repo = $_POST['repo'] ? ($_SESSION['repo'] = $_POST['repo']) : $_SESSION['repo'];
$_SESSION['pagelen'] = $_POST['pagelen'] ? $_POST['pagelen'] : 50;

// New instance
$bbissuemaker = new BBIssueMaker($login, $password);

if (is_array($tasks)) {
	foreach ($tasks as $task) {
		$result = $bbissuemaker->request('https://bitbucket.org/api/1.0/repositories/'.$login.'/'.$repo.'/issues/', array(
			'title' => $task['title'],
			'content' => $task['content'],
			'kind' => 'task',
			'status' => 'resolved',
			'priority' => 'major',
		));
	}
}
include_once 'home.html';