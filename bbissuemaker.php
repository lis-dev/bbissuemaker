﻿<?
/**
 * Class makes compound to BitBucket.org server
 * Makes request to URL
 */
class BBIssueMaker {
	// Bitbucket username
	private $user;
	// Bitbucket password
	private $password;
	/**
	 * Default constructor
	 * 
	 * @param string $user Bitbucket username
	 * @param string $password Bitbucket password
	 * @return self
	 */
	function __construct($user = NULL, $password = NULL) {
		$user AND $this->user = $user;
		$password AND $this->password = $password;
		return $this;
	}
	
	function request($url, $post = array(), $post_method = CURLOPT_POST) {
		try {
			if (!$this->user)
				throw new Exception("Login is required");
			if (!$this->password)
				throw new Exception("Password is required");
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_USERPWD, $this->user . ":" . $this->password);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			if ($post) {
				curl_setopt($ch, $post_method, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			}
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			$resp = curl_exec($ch);
			// validate CURL status
			if (curl_errno($ch))
				throw new Exception(curl_error($ch));

			return $resp;
		} catch(Exception $e) {
			return "Exception " . $e->getMessage();
		}
		isset($ch) and curl_close($ch);
	}
}