$(function() {
	$.post('/ajax.php', {
		'get_last_commits' : 50
	}, function(res) {
		var result = '';
		if (res.values) {
			for (i in res.values) {
				result += '<li data-hash="' + res.values[i].hash + '"><div class="alert alert-info"><i>' + res.values[i].date + '</i> ' + res.values[i].message + '</div></li>';
			}
		}
		$('#commits_list').html(result);
	}, 'json').error(function(e) {
		console.log(e.responseText);
	});

	make_sortable_list($("ol.sortable_list"));
	$('#add_new_issue').click(function() {
		var new_issue_block = $('.issue:last').clone();
		new_issue_block.find('.titles').val('');
		new_issue_block.find('.content').val('');
		new_issue_block.find('.hashes').val('');
		var ol = new_issue_block.children('ol');
		var temp_id = +new Date();
		ol.empty().attr('temp_id', temp_id);
		make_sortable_list(ol);
		$('.issue:last').after(new_issue_block);
	});
	
	$('#issues').submit(function() {
		var content = '';
		var hashes = '';
		var content = '';
		$('.issue ol').each(function() {
			content = '';
			hashes = '';
			content = '';
			$(this).children('li').each(function() {
				content += $(this)
					.html()
					.replace(/<i>.+<\/i>/ig, '')
					.replace(/\"/g, "&#34;")
					.trim();
				content += "\n";
				hashes += "bbissuemaker:" + $(this).data('hash') + "\n";
			});
			//content = content.replace(/\n\n/ig, "\n");
			if ( ! $(this).siblings('.content').length) {
				$(this).after('<input type="hidden" class="content" name="content[]" value="' + content + '" />');
				$(this).after('<input type="hidden" class="hashes" name="hashes[]" value="' + hashes + '" />');
			} else {
				$(this).siblings('.content').val(content);
				$(this).siblings('.hashes').val(hashes);
			}
		});
		return true;
	});
}).on('click', '.toggle', function() {
	$(this).parents('.issue').children('.sortable_list').toggle('fast');
	var new_class = $(this).hasClass('glyphicon-minus') ? 'glyphicon-plus' : 'glyphicon-minus';
	$(this).removeClass('glyphicon-minus glyphicon-plus').addClass(new_class);
	return false;
});

function make_sortable_list(list_el) {
	var oldContainer;
	return list_el.sortable({
		group : 'nested',
		afterMove : function(placeholder, container) {
			if (oldContainer != container) {
				if (oldContainer)
					oldContainer.el.removeClass("active");
				container.el.addClass("active");

				oldContainer = container;
			}
		},
		onDrop : function(item, container, _super) {
			container.el.removeClass("active");
			_super(item);
		}
	});
}
