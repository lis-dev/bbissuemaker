<?
session_start();
require_once './bbissuemaker.php';
// Login, password and repository
$login = $_SESSION['login'];
$password = $_SESSION['password'];
$repo = $_SESSION['repo'];
$pagelen = $_SESSION['pagelen'];

// New instance
$bbissuemaker = new BBIssueMaker($login, $password);

// Get new untreated commits 
if ($_POST['get_last_commits']) {
	$issues = $bbissuemaker->request('https://bitbucket.org/api/1.0/repositories/'.$login.'/'.$repo.'/issues/');
	$issues = json_decode($issues, 1);
	
	// Get last commits
	$commits = $bbissuemaker->request('https://bitbucket.org/api/2.0/repositories/'.$login.'/'.$repo.'/commits/?pagelen='.$pagelen);
	$commits = json_decode($commits, 1);
	$commits['values'] = array_reverse($commits['values']);
	if (is_array($issues['issues']) AND is_array($commits['values'])) {
		$commits_delete = array();
		// If mark has been found, don't show this commit
		foreach($commits['values'] as $commit) {
			foreach($issues['issues'] as $issue) {
				if (mb_stripos($issue['content'], 'bbissuemaker:'.$commit['hash']) !== FALSE) {
					$commits_delete[$commit['hash']] = TRUE;
				}
			}
		}
		// Delete unnecessary commits
		if ($commits_delete) {
			foreach($commits['values'] as $i => $commit) {
				if ($commits_delete[$commit['hash']]) {
					unset($commits['values'][$i]);
				}
			}
		}
	}
	
	$response = json_encode($commits);
	echo $response;
}
